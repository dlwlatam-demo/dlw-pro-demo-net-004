using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;

namespace MiProyecto.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HelloWorldController : ControllerBase
    {
        private readonly IHostEnvironment _env;

        public HelloWorldController(IHostEnvironment env)
        {
            _env = env;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var message = $"¡Hola Mundo desde {_env.EnvironmentName}!";
            return Ok(message);
        }
    }
}
